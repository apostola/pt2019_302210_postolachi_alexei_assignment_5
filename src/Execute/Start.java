package Execute;

import ProblemPackage.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Start {
    public static void main(String[] args) throws IOException {
        DataProcessing dataPro = new DataProcessing();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Stream<String> rows = Files.lines(Paths.get("src/activities.txt"));
        rows
                .map(x -> x.split("\t\t"))
                .forEach(x -> {
                    try {
                        dataPro.add(format.parse(x[0]), format.parse(x[1]), x[2]);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                });

        rows.close();


        System.out.println("Days logged:"+dataPro.computeDays());

        Map<String, Long> activ = dataPro.countActivities();
        System.out.println("\n\nCounting Activities");
        for (Map.Entry entry: activ.entrySet()) {
            Long value = (Long)entry.getValue();
            System.out.println(entry.getKey()+" "+value);
        }

        Map<String, Long> totalTime = dataPro.getTotalTime();
        System.out.println("\n\nTotal time");
        for (Map.Entry entry: totalTime.entrySet()) {
            Long value = (Long)entry.getValue();
            System.out.println(entry.getKey()+" "+value);
        }

        Map<MonitoredData, Long> diffMap = dataPro.getDifference();
        System.out.println("\n\nComputing time difference");
        for (Map.Entry entry: diffMap.entrySet()) {
            Long value = (Long)entry.getValue();
            System.out.println(entry.getKey()+" "+value);
        }

        Map<LocalDate, Map<String, Long>> activityPerDay = dataPro.activityPerDays();
        System.out.println("\n\nComputing per days");
        for (Map.Entry entry: activityPerDay.entrySet()) {
            Map<String, Long> value = (Map<String, Long>)entry.getValue();
            for (Map.Entry data: value.entrySet())
            {
                System.out.println(entry.getKey()+" "+data.getKey()+" "+data.getValue());
            }
        }

        System.out.println("\n\nFiltered time");
        List<String> list = dataPro.displayByFilteredTime();
        System.out.println(list.toString());
    }
}
