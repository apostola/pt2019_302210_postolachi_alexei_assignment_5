package ProblemPackage;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class DataProcessing {
    private ArrayList<MonitoredData> logs = new ArrayList<MonitoredData>();

    public DataProcessing() {
        System.out.println("Data processed\n\n");
    }

    public void add(Date dataStart, Date dataEnd, String log) {
        logs.add(new MonitoredData(dataStart, dataEnd, log));
    }

    public long computeDays() {
       return logs.stream()
               .collect(Collectors.groupingBy(log -> log.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()))
               .size();
    }

    //https://stackoverflow.com/questions/23925315/count-int-occurrences-with-java8
    public Map<String, Long> countActivities() {
        return logs.stream()
                .collect(Collectors.groupingBy(log -> log.getActivity(), Collectors.counting()));
    }

    public Map<MonitoredData, Long> getDifference() {
            return logs.stream()
                    .collect(Collectors.toMap(log -> log, MonitoredData::getTimeDifference));
    }

    //https://stackoverflow.com/questions/32312876/ignore-duplicates-when-producing-map-using-streams
    public Map<String, Long> getTotalTime() {
        return logs.stream()
                .collect(Collectors.toMap(MonitoredData::getActivity, MonitoredData::getTimeDifference,
                        (x, y) -> {
                            return x + y;
                        }));
    }

    public List<String> displayByFilteredTime() {
        Map<String, int[]> filter = logs.stream()
                .collect(Collectors.toMap(MonitoredData::getActivity,
                        time -> time.getTimeDifference() < 3_00 ? new int[]{1, 0}: new int[]{0, 1},
                        (oldValue, newValue) -> {
                        oldValue[0] += newValue[0];
                        oldValue[1] += newValue[1];
                        return oldValue;
                        }));

        return filter.entrySet().stream()
                .filter(d -> d.getValue()[0] / (d.getValue()[0] + d.getValue()[1]) > 0.9)
                .map(x -> x.getKey())
                .collect(Collectors.toList());
        }

    public Map<LocalDate, Map<String, Long>> activityPerDays() {
        return logs.stream()
                .collect(Collectors.groupingBy(day -> day.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), Collectors.groupingBy(day -> day.getActivity(), Collectors.counting())));
    }
}