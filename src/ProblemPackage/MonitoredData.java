package ProblemPackage;
import java.util.Date;


public class MonitoredData {
    private Date startTime;
    private Date endTime;
    private String activity;
    private long timeDifference;

    public MonitoredData(Date startTime, Date endTime, String activity) {
        this.activity = activity;
        this.endTime = endTime;
        this.startTime = startTime;
        timeDifference = (endTime.getTime() - startTime.getTime())/1000;
    }

    public String toString() {
        return "Start time: "+startTime.toString()+" End time: "+endTime.toString()+" Activity: "+activity;
    }

    public Date getStartTime() {return startTime;}
    public Date getEndTime() {return endTime;}
    public String getActivity() {return activity;}
    public long getTimeDifference() {return timeDifference;}

    public boolean equals(Object obj) {
        if (obj != null && obj instanceof MonitoredData) {
            MonitoredData data = (MonitoredData)obj;
            return activity.equals(data.getActivity())
                    && (startTime.equals(data.getStartTime())
                    && (endTime.equals(data.getEndTime())));
        }
        return false;
    }

    @Override
    public int hashCode() {
        return activity.hashCode() + startTime.hashCode() + endTime.hashCode();
    }
}
